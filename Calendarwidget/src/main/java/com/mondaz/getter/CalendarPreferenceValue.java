package com.mondaz.getter;

public class CalendarPreferenceValue {

	public static final String PREF_ACTIVE_CALENDARS = "activeCalendars";
	public static final String PREF_EVENT_RANGE= "eventRange";
	public static final String PREF_EVENT_RANGE_DEFAULT = "30";
	public static final String PREF_WIDGET_BACKGROUND_COLOR = "widgetbackgroundColor";
	public static final String PREF_WIDGET_BACKGROUND_COLOR_DEFAULT = "#000000";
	public static final String PREF_CALENDAR_BACKGROUND_COLOR = "calendarbackgroundColor";
	public static final String PREF_CALENDAR_BACKGROUND_COLOR_DEFAULT = "#FFAB00";
	public static final String PREF_TEXT_BACKGROUND_COLOR = "textbackgroundColor";
	public static final String PREF_TOMORROW_BACKGROUND_COLOR = "tomorrowtextbackgroundColor";
	public static final String PREF_TOMORROW_BACKGROUND_COLOR_DEFAULT = "#009ACD";
	public static final String PREF_TEXT_BACKGROUND_COLOR_DEFAULT = "#ccffffff";
	public static final String PREF_TEXT_SIZE = "textsize";
	public static final String PREF_TEXT_SIZE_DEFAULT = "12";
	public static final String WIDGET_BACKGROUND_OPACITY = "backgroundopacity";
	public static final String WIDGET_BACKGROUND_OPACITY_DEFAULT = "B2";
	public static final String WIDGET_TIME_WITH_AM_PM = "useAmPmwithTime";
	public static final String WIDGET_TIME_WITH_AM_PM_DEFAULT = "false";
	public static final String NUMBER_OF_EVENTS = "noofevents";
	public static final String NUMBER_OF_EVENTS_DEFAULT = "3";
	public static final String PREF_SHOW_CALENDAR = "showcalendar";
	public static final String PREF_SHOW_CALENDAR_DEFAULT = "false";
	public static final String PREF_NOTIFICATION_TIME = "notificationTime";
	public static final int   PREF_NOIFICAION_TIME_DEFAULT = 5;
	public static final String PREF_WIDGET_ROUNDED_CORNER = "roundedCorners";
	public static final String WIDGET_ROUNDED_CORNER ="widgetRoundedCorner";
	public static final boolean WIDGET_ROUNDED_CORNER_DEFAULT = false;


	private CalendarPreferenceValue() {
		// prohibit instantiation
	}
}
