package com.mondaz.getter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;


public class CalendarEvent {
	private int id;
	private long dateMillis;
	private long endDateMillis;
	private String title;
	private boolean allDay;

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	

	public void setDateMillis(long dateMillis) {	
		this.dateMillis = dateMillis;
	}

	public long getDateMillis() {
		return dateMillis;
	}
	
	public void setDateEndMillis(long endDateMillis) {	
		this.endDateMillis = endDateMillis;
	}

	public long getDateEndMillis() {
		return endDateMillis;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitle() {
		return title;
	}
	
	public boolean isAllDay() {
		return allDay;
	}

	public void setAllDay(boolean allDay) {
		this.allDay = allDay;
	}
	
	public String toString(){
		return getId() + ": "+getTitle() + " " + new Date(getDateMillis()).toString();
	}
	
	public String getFormatedTime() {
		Date d = new Date(dateMillis);
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM HH:mm");
		SimpleDateFormat allDayFormatter = new SimpleDateFormat("dd MMM");
		String formatedDate = formatter.format(d);
		
		if(allDay){		
			Date start = new Date(dateMillis);
			Date end = new Date(endDateMillis);
		    
			String allDayFormatedDate = allDayFormatter.format(start);
			String localFormatedDay = convertUtcTolocal(allDayFormatedDate);
			
			DateTime eventEndingTime = new DateTime(end);	
			DateTime tomorrow = new DateTime();
			tomorrow.withTimeAtStartOfDay();
			if(DateTimeComparator.getDateOnlyInstance().compare(eventEndingTime, tomorrow) == 0){
				return null;
			}
			
			return localFormatedDay;
		}
		return formatedDate;
	}
	
	public String getTimeWithAM() {
		Date d = new Date(dateMillis);
		SimpleDateFormat formatter = new SimpleDateFormat("dd MMM hh:mm a");
		SimpleDateFormat allDayFormatter = new SimpleDateFormat("dd MMM");
		String formatedDate = formatter.format(d);
		
		if(allDay){		
			String allDayFormatedDate = allDayFormatter.format(d);
			String localFormatedDay = convertUtcTolocal(allDayFormatedDate);						
			return localFormatedDay;
		}
		return formatedDate;
	}

	public String convertUtcTolocal(String UTC){
		String time = null;
		try {
		SimpleDateFormat Fmt = new SimpleDateFormat(
				"dd MMM");
		Date parse;
		
			parse = Fmt.parse(UTC);
			TimeZone currenttimeZone = TimeZone
					.getDefault();
			SimpleDateFormat destFmt = new SimpleDateFormat("dd MMM");
			destFmt.setTimeZone(currenttimeZone);
			time = destFmt.format(parse);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	
		return time;
	}
	
}
