package com.mondaz.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.mondaz.widgettest.CheckWidget;

public class DateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i("DATE RECEIVER", "OK");

        Toast.makeText(context, "widgets", Toast.LENGTH_SHORT).show();

        CheckWidget.updateAllWidgets(context);


    }
}