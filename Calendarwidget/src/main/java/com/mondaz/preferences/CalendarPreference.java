package com.mondaz.preferences;

import java.util.HashSet;
import java.util.Set;

import android.content.ContentResolver;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.provider.CalendarContract.Calendars;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_ACTIVE_CALENDARS;

import com.mondaz.widgettest.CheckWidget;
import com.mondaz.widgettest.R;

public class CalendarPreference extends PreferenceActivity{
	private static final String CALENDAR_ID = "calendarId";	
	private static final String[] PROJECTION_SUB = new String[] { Calendars._ID,		
		Calendars.NAME, Calendars.CALENDAR_COLOR, Calendars.ACCOUNT_NAME, Calendars.ACCOUNT_TYPE };
	private Set<String> initialActiveCalendars;
	private static final String ACCOUNT_CATAGORY_TITLE = "catagorytitle";

    CheckBoxPreference mCheckBoxPreference;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.calendaraccounts);
		SharedPreferences prefs = getPreferenceManager().getSharedPreferences();
		initialActiveCalendars = prefs.getStringSet(PREF_ACTIVE_CALENDARS, null);
		populatePreferenceScreen(initialActiveCalendars);					
	}
	
    private void populatePreferenceScreen(Set<String> activeCalendars) {
        Cursor cursor = createLoadedCursor();
        PreferenceScreen screen = getPreferenceScreen();
        SharedPreferences checkPrefs = getSharedPreferences(ACCOUNT_CATAGORY_TITLE, 0);
		if (cursor == null) {
			return;
		}
		for (int i = 0; i < cursor.getCount(); i++) {
			cursor.moveToPosition(i);
			int calendarId = cursor.getInt(0);
			PreferenceCategory accountCatagory = new PreferenceCategory(this);
                String value = checkPrefs.getString(ACCOUNT_CATAGORY_TITLE, "05-90");
				if(!value.contains(cursor.getString(3))){
					accountCatagory.setTitle(cursor.getString(3));
					screen.addPreference(accountCatagory);
					//Save the last value in SP db
					Editor mEditor = checkPrefs.edit();
					mEditor.putString(ACCOUNT_CATAGORY_TITLE, cursor.getString(3));
					mEditor.commit();			
				}
				
			
			CheckBoxPreference checkboxPref = new CheckBoxPreference(this);
			if(cursor.getString(1) != null)
			    checkboxPref.setTitle(cursor.getString(1));
			else
				checkboxPref.setTitle("Local Calendar");
			checkboxPref.setIcon(createDrawable(cursor.getInt(2)));
			
			checkboxPref.getExtras().putInt(CALENDAR_ID, calendarId);
			checkboxPref.setChecked(activeCalendars == null
					|| activeCalendars.contains(String.valueOf(calendarId)));
			getPreferenceScreen().addPreference(checkboxPref); 
		}
	}
    
        @Override
	       public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
	     	return true;
	    }

	
        private Cursor createLoadedCursor() { 
        return  getContentResolver().query(Calendars.CONTENT_URI, 
        		PROJECTION_SUB, Calendars.VISIBLE + " = 1", null, 
        		                  Calendars._ID + " ASC");
	   }
        
    	@Override
    	public void onPause() {
    		super.onPause();   	
    		HashSet<String> selectedCalendars = getSelectedCalenders();  
    		if (!selectedCalendars.equals(initialActiveCalendars)) {
    			persistSelectedCalendars(selectedCalendars);
    			CheckWidget.updateAllWidgets(this);
    		}
    	}

        private void persistSelectedCalendars(HashSet<String> prefValues) {
        	
            SharedPreferences prefs = getPreferenceManager().getDefaultSharedPreferences(this);
    		Editor editor = prefs.edit();
            editor.putStringSet(PREF_ACTIVE_CALENDARS, prefValues);
    		editor.commit();
    	}

        private HashSet<String> getSelectedCalenders() {
        	PreferenceScreen preferenceScreen = getPreferenceScreen();
    		int prefCount = preferenceScreen.getPreferenceCount();
            HashSet<String> prefValues = new HashSet<String>();
            for (int i = 0; i < prefCount; i++) {
    			Preference pref = preferenceScreen.getPreference(i);
    			if (pref instanceof CheckBoxPreference) {
    				CheckBoxPreference checkPref = (CheckBoxPreference) pref;
    				if (checkPref.isChecked()) {
    					prefValues.add(String.valueOf(checkPref.getExtras().getInt(CALENDAR_ID)));
    				}
    			}
    		}
    		return prefValues;
    	}
        
        private Drawable createDrawable(int color) {
            Drawable drawable = getResources().getDrawable(R.drawable.prefs_calendar_entry);
            drawable.setColorFilter(new LightingColorFilter(0x0, color));
            return drawable;
        }
        
        
}
