package com.mondaz.preferences;

import static com.mondaz.getter.CalendarPreferenceValue.NUMBER_OF_EVENTS;
import static com.mondaz.getter.CalendarPreferenceValue.NUMBER_OF_EVENTS_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_CALENDAR_BACKGROUND_COLOR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_CALENDAR_BACKGROUND_COLOR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_EVENT_RANGE_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_SHOW_CALENDAR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_SHOW_CALENDAR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TEXT_BACKGROUND_COLOR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TEXT_BACKGROUND_COLOR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TEXT_SIZE;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TEXT_SIZE_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_WIDGET_BACKGROUND_COLOR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_WIDGET_BACKGROUND_COLOR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_WIDGET_ROUNDED_CORNER;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_BACKGROUND_OPACITY;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_BACKGROUND_OPACITY_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_ROUNDED_CORNER;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_ROUNDED_CORNER_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_TIME_WITH_AM_PM;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_TIME_WITH_AM_PM_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_EVENT_RANGE;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_EVENT_RANGE_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_NOIFICAION_TIME_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TOMORROW_BACKGROUND_COLOR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TOMORROW_BACKGROUND_COLOR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_NOTIFICATION_TIME;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.provider.CalendarContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.larswerkman.holocolorpicker.ColorPicker.OnColorChangedListener;
import com.larswerkman.holocolorpicker.OpacityBar;
import com.larswerkman.holocolorpicker.SVBar;
import com.mondaz.getter.CalendarPreferenceValue;
import com.mondaz.widgettest.CheckWidget;
import com.mondaz.widgettest.R;

public class Calendar_Settings extends PreferenceActivity implements OnColorChangedListener, OnSharedPreferenceChangeListener,
		OnPreferenceChangeListener {
	private PreferenceCategory mPreferenceCategory, mPreferenceCategoryForColor, mPreferenceCategoryForDisplay;
	private Preference account_settings,	changeBackgroundColor, changeTextColor, changeCalendarColor, setToDefault, changeTomorrowColor;
	private SeekBarPreference mSeekBarPreference;
	private ListPreference listPreference;
	private CheckBoxPreference roundedCornersCheckBox;
	private EditTextPreference itmstoShow, textSize, notificationTime;
	private static final String KEY_EDIT_TEXT_PREFERENCE = "no_of_items";
	private static final String KEY_TEXT_SIZE = "text_size";
	private static final String KEY_NOTIFICATION_TIME = "set_notification_time";
	private ColorPicker picker;
	private SVBar svBar;
	private OpacityBar opacityBar;
	private  boolean isClicked = false;
	private  boolean hasChanged = false;
	private  boolean listClicked = false;
	private boolean timeChanged =false;

	private static final int WIDGET_BACKGROUND = 1;
	private static final int TEXT_BACKGROUND = 2;
	private static final int CALENDAR_BACKGROUND = 3;
	private static final int TOMORROW_COLOR = 4;

	//All SharedPreferences
	private SharedPreferences holdOpacity, timePref, mPrefTextSize, mPrefNoOfEvents,tomorrowTextColor,
	                          mPrefCalBG, mTextBG, mWidgetBG, mSharedPreferences, editTextPrefKey, mPrefShowCalendar, listPrefs, mNotificatonTime, mroundedCorners;

	@SuppressWarnings("deprecation")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferences);
		listPrefs = getSharedPreferences(PREF_EVENT_RANGE, 0);
		holdOpacity = getSharedPreferences(WIDGET_BACKGROUND_OPACITY, MODE_PRIVATE);
		timePref = getSharedPreferences(WIDGET_TIME_WITH_AM_PM, 0);
		mWidgetBG = getSharedPreferences(PREF_WIDGET_BACKGROUND_COLOR, 0);
		mTextBG = getSharedPreferences(PREF_TEXT_BACKGROUND_COLOR, 0);
		mPrefCalBG = getSharedPreferences(PREF_CALENDAR_BACKGROUND_COLOR, 0);
		mPrefNoOfEvents = getSharedPreferences(NUMBER_OF_EVENTS, 0);
		mPrefTextSize = getSharedPreferences(PREF_TEXT_SIZE, 0);
		mPrefShowCalendar = getSharedPreferences(PREF_SHOW_CALENDAR, 0);
		mNotificatonTime = getSharedPreferences(PREF_NOTIFICATION_TIME,0);
		tomorrowTextColor =getSharedPreferences(PREF_TOMORROW_BACKGROUND_COLOR, 0);
        mroundedCorners =getSharedPreferences(PREF_WIDGET_ROUNDED_CORNER,0);


		setToDefault = (Preference)findPreference("set_to_default");
		mSeekBarPreference = (SeekBarPreference) findPreference("seekbarPref");

		mSeekBarPreference.setOnPreferenceChangeListener(this);
		mPreferenceCategory = (PreferenceCategory) findPreference("calendar_settings");
		account_settings = (Preference) findPreference("calendar_accounts_settings");

		mPreferenceCategory.addPreference(account_settings);
		mPreferenceCategoryForColor = (PreferenceCategory) findPreference("color_setting");
		//changecurrent_event_color = (Preference) findPreference("current_day_color");
		changeBackgroundColor = (Preference) findPreference("background_color");
		changeTextColor = (Preference) findPreference("textview_color");
		changeCalendarColor = (Preference) findPreference("day_month_color");
		changeTomorrowColor = (Preference) findPreference("tomorrow_color");
		notificationTime =(EditTextPreference) findPreference("set_notification_time");
        roundedCornersCheckBox =(CheckBoxPreference) getPreferenceManager().findPreference("rounded edge");
		listPreference = (ListPreference) findPreference(PREF_EVENT_RANGE);
		listPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				listClicked = true;
				return true;
			}
		});



		if(mroundedCorners.getBoolean(WIDGET_ROUNDED_CORNER, WIDGET_ROUNDED_CORNER_DEFAULT))
		{
			roundedCornersCheckBox.setChecked(true);
		}
		else
		{
			roundedCornersCheckBox.setChecked(false);
		}


		roundedCornersCheckBox.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				Editor editor = mroundedCorners.edit();
				editor.putBoolean(WIDGET_ROUNDED_CORNER, Boolean.parseBoolean(newValue.toString()));
				editor.commit();
				updateWidget();
				return true;
			}
		});



		mPreferenceCategoryForDisplay = (PreferenceCategory) findPreference("Display_setting");




		CheckBoxPreference checkboxPref = (CheckBoxPreference) getPreferenceManager().findPreference("AmPmcheckboxPref");

		if(!timePref.getString(WIDGET_TIME_WITH_AM_PM, WIDGET_TIME_WITH_AM_PM_DEFAULT).contains("false")){
			checkboxPref.setChecked(true);
		}else{
			checkboxPref.setChecked(false);
		}
	    checkboxPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
	        public boolean onPreferenceChange(Preference preference, Object newValue) {
	        	Editor timeEditor = timePref.edit();
	        	timeEditor.putString(WIDGET_TIME_WITH_AM_PM, newValue.toString());
	        	timeEditor.commit();
	        	updateWidget();
	            return true;
	        }
	    });

	    CheckBoxPreference showCalPref = (CheckBoxPreference) getPreferenceManager().findPreference("show_calendar_in_background");

	    if(!mPrefShowCalendar.getString(PREF_SHOW_CALENDAR, PREF_SHOW_CALENDAR_DEFAULT).contains("false")){
	    	showCalPref.setChecked(true);
		}else{
			showCalPref.setChecked(false);
		}

	    showCalPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
	        public boolean onPreferenceChange(Preference preference, Object newValue) {
	        	Editor calEditor = mPrefShowCalendar.edit();
	        	calEditor.putString(PREF_SHOW_CALENDAR, newValue.toString());
	        	calEditor.commit();
	        	updateWidget();
	            return true;
	        }
	    });

		itmstoShow = (EditTextPreference) findPreference("no_of_items");
		itmstoShow
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						isClicked = true;
						return true;
					}
				});

		textSize = (EditTextPreference) findPreference("text_size");
		textSize
		.setOnPreferenceClickListener(new OnPreferenceClickListener() {

			@Override
			public boolean onPreferenceClick(Preference preference) {
				hasChanged = true;
				return true;
			}
		});

		account_settings
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						Intent intent = new Intent(Calendar_Settings.this,
								CalendarPreference.class);
						startActivity(new Intent(intent));
						return true;
					}
				});

		changeBackgroundColor
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						colorDialog(WIDGET_BACKGROUND).show();
						return true;
					}
				});
		changeTextColor
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						colorDialog(TEXT_BACKGROUND).show();
						return true;
					}
				});
		changeCalendarColor
				.setOnPreferenceClickListener(new OnPreferenceClickListener() {

					@Override
					public boolean onPreferenceClick(Preference preference) {
						colorDialog(CALENDAR_BACKGROUND).show();
						return true;
					}
				});


		changeTomorrowColor.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {


				colorDialog(TOMORROW_COLOR).show();
				return true;
			}
		});




		setToDefault
		      .setOnPreferenceClickListener(new OnPreferenceClickListener() {

			        @Override
			       public boolean onPreferenceClick(Preference preference) {
			        	AlertDialog mDialog;
			        	AlertDialog.Builder mBuilder = new AlertDialog.Builder(Calendar_Settings.this);
			        	mBuilder.setIcon(R.drawable.dft);
			        	mBuilder.setTitle("Set Default");
			        	mBuilder.setMessage("Do you like to make all settings to default ?");
			        	mBuilder.setPositiveButton(" Set ", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {
								setAllToDefault();
							}
						});
			        	mBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog, int which) {

							}
						});
			        	mDialog = mBuilder.create();
			        	mDialog.show();
				       return true;
			}
		});


//
//		notificationTime.setOnPreferenceClickListener(new OnPreferenceClickListener() {
//			@Override
//			public boolean onPreferenceClick(Preference preference) {
//
//
//
//				timeChanged = true;
//
//				return true;
//			}
//		});





	}

	private String getCalendarUriBase(boolean eventUri) {
		Uri calendarURI = null;
		try {
			if (android.os.Build.VERSION.SDK_INT <= 7) {
				calendarURI = (eventUri) ? Uri.parse("content://calendar/") : Uri.parse("content://calendar/calendars");
			} else {
				calendarURI = (eventUri) ? Uri.parse("content://com.android.calendar/") : Uri
						.parse("content://com.android.calendar/calendars");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return calendarURI.toString();
	}

	private AlertDialog colorDialog(final int key) {
		LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		View mView = inflater.inflate(R.layout.colorpicker, null);
		AlertDialog mDialog;
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setIcon(R.drawable.colorpicker);
		builder.setTitle("COLOR PICKER");
		builder.setView(mView);

		String color;
		picker = (ColorPicker) mView.findViewById(R.id.picker);

		switch (key) {
		case WIDGET_BACKGROUND:
			color = mWidgetBG.getString(PREF_WIDGET_BACKGROUND_COLOR, PREF_WIDGET_BACKGROUND_COLOR_DEFAULT);
			if(color != null)
		    picker.setOldCenterColor(Color.parseColor(color));
			break;
		case TEXT_BACKGROUND:
			color = mTextBG.getString(PREF_TEXT_BACKGROUND_COLOR, PREF_TEXT_BACKGROUND_COLOR_DEFAULT);
			if(color != null)
		    picker.setOldCenterColor(Color.parseColor(color));
			break;
		case CALENDAR_BACKGROUND:
			color = mPrefCalBG.getString(PREF_CALENDAR_BACKGROUND_COLOR, PREF_CALENDAR_BACKGROUND_COLOR_DEFAULT);
			if(color != null){
				 picker.setOldCenterColor(Color.parseColor(color));
				 Log.v("TODAY BG COLOR", color);
			}
			break;
			case TOMORROW_COLOR:
				color =tomorrowTextColor.getString(PREF_TOMORROW_BACKGROUND_COLOR, PREF_TOMORROW_BACKGROUND_COLOR_DEFAULT);
				if(color != null){
					picker.setOldCenterColor(Color.parseColor(color));
					Log.v("TOMORROW BG COLOR", color);
				}
			break;
		}

		svBar = (SVBar) mView.findViewById(R.id.svbar);
		opacityBar = (OpacityBar) mView.findViewById(R.id.opacitybar);

		picker.addSVBar(svBar);
		picker.addOpacityBar(opacityBar);
		picker.setOnColorChangedListener(this);
		builder.setPositiveButton("PICK",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						String strColor = String.format("#%06X",
								0xFFFFFF & picker.getColor());
						picker.setOldCenterColor(picker.getColor());

						switch (key) {
						case WIDGET_BACKGROUND:

							Editor mWBG = mWidgetBG.edit();
							mWBG.putString(PREF_WIDGET_BACKGROUND_COLOR, strColor);
							mWBG.commit();
							updateWidget();
							break;
						case TEXT_BACKGROUND:

							Editor mTxtBG = mTextBG.edit();
							mTxtBG.putString(PREF_TEXT_BACKGROUND_COLOR, strColor);
							mTxtBG.commit();
							updateWidget();
							break;
						case CALENDAR_BACKGROUND:

							Editor mCLBG = mPrefCalBG.edit();
							mCLBG.putString(PREF_CALENDAR_BACKGROUND_COLOR, strColor);
							mCLBG.commit();
							updateWidget();
							break;

							case TOMORROW_COLOR:
								Editor mTomoroWColor= tomorrowTextColor.edit();
								mTomoroWColor.putString(PREF_TOMORROW_BACKGROUND_COLOR, strColor);
								mTomoroWColor.commit();
								updateWidget();
								break;
						}


					}
				});
		builder.setNegativeButton("CLOSE", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

					}
				});
		mDialog = builder.create();
		return mDialog;
	}

	@Override
	public void onColorChanged(int color) {

	}

	@Override
	protected void onResume() {
		super.onResume();
		getPreferenceScreen().getSharedPreferences()
				.registerOnSharedPreferenceChangeListener(this);
		updatePreference(KEY_EDIT_TEXT_PREFERENCE);
		updatePreference(KEY_TEXT_SIZE);
		updatePreference(PREF_EVENT_RANGE);
		updatePreference(KEY_EDIT_TEXT_PREFERENCE);
		if(holdOpacity.contains(WIDGET_BACKGROUND_OPACITY)){
			mSeekBarPreference.setProgress(Integer.parseInt(
					  holdOpacity.getString(WIDGET_BACKGROUND_OPACITY, WIDGET_BACKGROUND_OPACITY_DEFAULT), 16));
		}

	}

	@Override
	protected void onPause() {
		super.onPause();
		// Unregister the listener whenever a key changes
		getPreferenceScreen().getSharedPreferences()
				.unregisterOnSharedPreferenceChangeListener(this);
		//getListData();
		updateWidget();
	}


	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		updatePreference(key);
	}

	private void updatePreference(String key) {
		if (key.equals(KEY_EDIT_TEXT_PREFERENCE)) {
			Preference preference = findPreference(key);
			if (preference instanceof EditTextPreference) {
				EditTextPreference editTextPreference = (EditTextPreference) preference;

				if (isClicked) {
					Editor mEditor = mPrefNoOfEvents.edit();
					mEditor.putString(NUMBER_OF_EVENTS, editTextPreference.getText());
					mEditor.commit();
					updateWidget();
				}
				else{
					if(mPrefNoOfEvents.contains(NUMBER_OF_EVENTS)){
						String eventNumber = mPrefNoOfEvents.getString(NUMBER_OF_EVENTS, NUMBER_OF_EVENTS_DEFAULT);
						editTextPreference.setText(eventNumber);
					}
				}
			}
		}
		else if(key.equals(KEY_TEXT_SIZE)){
			Preference preference = findPreference(key);
			if (preference instanceof EditTextPreference) {
				EditTextPreference editTextPreference = (EditTextPreference) preference;
				if (hasChanged) {
					Editor mEditor = mPrefTextSize.edit();
					mEditor.putString(PREF_TEXT_SIZE, editTextPreference.getText());
					mEditor.commit();
					updateWidget();
				}
				else{
					if(mPrefTextSize.contains(PREF_TEXT_SIZE)){
						String eventTextSize = mPrefTextSize.getString(PREF_TEXT_SIZE, PREF_TEXT_SIZE_DEFAULT);
						editTextPreference.setText(eventTextSize);
					}
				}
			}
		}

		 if(key.equals(PREF_EVENT_RANGE)){
			Preference preference = findPreference(key);
			if (preference instanceof ListPreference) {
				ListPreference listPref = (ListPreference) preference;
				if(listClicked){
					Editor mEditor = listPrefs.edit();
					mEditor.putString(PREF_EVENT_RANGE, listPref.getValue());
					mEditor.commit();
					updateWidget();
				}
				else{
					if(listPrefs.contains(PREF_EVENT_RANGE)){
						String eventRange = listPrefs.getString(PREF_EVENT_RANGE, PREF_EVENT_RANGE_DEFAULT);
						listPref.setValue(eventRange);
					}
				}
			}
		}


//		if(key.equals(KEY_NOTIFICATION_TIME))
//		{
//			Preference preference = findPreference(key);
//			if (preference instanceof EditTextPreference) {
//				EditTextPreference editTextPreference = (EditTextPreference) preference;
//
//				if (timeChanged) {
//					Editor mEditor = mNotificatonTime.edit();
//					mEditor.putInt(PREF_NOTIFICATION_TIME, Integer.parseInt(editTextPreference.getText()));
//					mEditor.commit();
//					updateWidget();
//				}
//				else{
//					if(mNotificatonTime.contains(PREF_NOTIFICATION_TIME)){
//						String eventNumber = mNotificatonTime.getInt(PREF_NOTIFICATION_TIME, PREF_NOIFICAION_TIME_DEFAULT)+"";
//						editTextPreference.setText(eventNumber);
//					}
//				}
//			}
//		}
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		int value = (Integer) newValue;
		Editor mEditor = holdOpacity.edit();
		if (value < 17) {
			mEditor.putString(WIDGET_BACKGROUND_OPACITY, "00");
			mEditor.commit();
			updateWidget();
			return true;
		}


		mEditor.putString(WIDGET_BACKGROUND_OPACITY, Integer.toHexString(value));
		mEditor.commit();
		updateWidget();
		return true;
	}


	//Set All SharedPreferences To Default
	private void setAllToDefault(){
		Editor mOpacityEditor = holdOpacity.edit();
		mOpacityEditor.putString(WIDGET_BACKGROUND_OPACITY, WIDGET_BACKGROUND_OPACITY_DEFAULT);
		mOpacityEditor.apply();

		Editor mTimeEditor = timePref.edit();
		mTimeEditor.putString(WIDGET_TIME_WITH_AM_PM, WIDGET_TIME_WITH_AM_PM_DEFAULT);
		mTimeEditor.apply();

		Editor mWidgetBGColorEditor = mWidgetBG.edit();
		mWidgetBGColorEditor.putString(PREF_WIDGET_BACKGROUND_COLOR, PREF_WIDGET_BACKGROUND_COLOR_DEFAULT);
		mWidgetBGColorEditor.apply();

		Editor mTextBGEditor = mTextBG.edit();
		mTextBGEditor.putString(PREF_TEXT_BACKGROUND_COLOR, PREF_TEXT_BACKGROUND_COLOR_DEFAULT);
		mTextBGEditor.apply();

		Editor mCalBGEditor = mPrefCalBG.edit();
		mCalBGEditor.putString(PREF_CALENDAR_BACKGROUND_COLOR, PREF_CALENDAR_BACKGROUND_COLOR_DEFAULT);
		mCalBGEditor.apply();

		Editor mEventNumberEditor = mPrefNoOfEvents.edit();
		mEventNumberEditor.putString(NUMBER_OF_EVENTS, NUMBER_OF_EVENTS_DEFAULT);
		mEventNumberEditor.apply();

		Editor mTextSizeEditor = mPrefTextSize.edit();
		mTextSizeEditor.putString(PREF_TEXT_SIZE, PREF_TEXT_SIZE_DEFAULT);
		mTextSizeEditor.apply();

		Editor mShowCalendareEditor = mPrefShowCalendar.edit();
		mShowCalendareEditor.putString(PREF_SHOW_CALENDAR, PREF_SHOW_CALENDAR_DEFAULT);
		mShowCalendareEditor.apply();

		Editor mEventRange = listPrefs.edit();
		mEventRange.putString(PREF_EVENT_RANGE, PREF_EVENT_RANGE_DEFAULT);
		mEventRange.commit();


		Editor mTomorrowColor = tomorrowTextColor.edit();
		mTomorrowColor.putString(PREF_TOMORROW_BACKGROUND_COLOR, PREF_TOMORROW_BACKGROUND_COLOR_DEFAULT);
		mTomorrowColor.commit();

		Editor roundedCorner = mroundedCorners.edit();
		roundedCorner.putBoolean(WIDGET_ROUNDED_CORNER, WIDGET_ROUNDED_CORNER_DEFAULT);
		roundedCorner.commit();

//		Editor notificationTime = mNotificatonTime.edit();
//		notificationTime.putInt(PREF_NOTIFICATION_TIME, PREF_NOIFICAION_TIME_DEFAULT);
//		notificationTime.commit();


		updateWidget();
		Thread mThread = new Thread(new Runnable() {

			@Override
			public void run() {
				startActivity(new Intent(Calendar_Settings.this, Calendar_Settings.class));
				overridePendingTransition(0, 0);
				finish();
			}
		});
		mThread.start();
	}

	private void updateWidget(){
		Thread updateThread = new Thread(new Runnable() {

			@Override
			public void run() {
				CheckWidget.updateAllWidgets(Calendar_Settings.this);
			}
		});
		updateThread.start();
	}

}
