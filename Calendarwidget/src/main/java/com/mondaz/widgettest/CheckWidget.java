package com.mondaz.widgettest;

import static com.mondaz.getter.CalendarPreferenceValue.PREF_CALENDAR_BACKGROUND_COLOR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_CALENDAR_BACKGROUND_COLOR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_EVENT_RANGE;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_EVENT_RANGE_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TEXT_BACKGROUND_COLOR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TEXT_BACKGROUND_COLOR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TEXT_SIZE;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TEXT_SIZE_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TOMORROW_BACKGROUND_COLOR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_TOMORROW_BACKGROUND_COLOR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_WIDGET_BACKGROUND_COLOR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_WIDGET_BACKGROUND_COLOR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_WIDGET_ROUNDED_CORNER;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_BACKGROUND_OPACITY;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_BACKGROUND_OPACITY_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_ROUNDED_CORNER;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_ROUNDED_CORNER_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_TIME_WITH_AM_PM;
import static com.mondaz.getter.CalendarPreferenceValue.WIDGET_TIME_WITH_AM_PM_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.NUMBER_OF_EVENTS;
import static com.mondaz.getter.CalendarPreferenceValue.NUMBER_OF_EVENTS_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_SHOW_CALENDAR;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_SHOW_CALENDAR_DEFAULT;
import static com.mondaz.getter.CalendarPreferenceValue.PREF_ACTIVE_CALENDARS;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.provider.CalendarContract.Attendees;
import android.provider.CalendarContract.Instances;
import android.text.format.DateUtils;
import android.util.Log;
import android.util.TypedValue;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.mondaz.getter.CalendarEvent;
import com.mondaz.preferences.Calendar_Settings;


public class CheckWidget extends AppWidgetProvider {
    private static final String EVENT_SORT_ORDER = "startDay ASC, allDay DESC, begin ASC ";
    private static final String EVENT_SELECTION = Instances.SELF_ATTENDEE_STATUS + "!="
            + Attendees.ATTENDEE_STATUS_DECLINED;
    private static final String[] PROJECTION = new String[]{Instances.EVENT_ID, Instances.TITLE,
            Instances.BEGIN, Instances.ALL_DAY, Instances.END};
    private Calendar mCalendar;

    private static final String CLOSING_BRACKET = " )";
    private static final String OR = " OR ";
    private static final String EQUALS = " = ";
    private static final String AND_BRACKET = " AND (";
    private Long start = 0L;
    private int event_row_counter;
    private boolean keyForAmPm = false;//Used to only check if the user check AM/Pm preference


    private String bagOpacity, BGColor, textColor, textSize, calendarColor, row_counter, showCalendar, tomorowColor;
    private SharedPreferences mSharedPreferences, mSharedPreferencesForEventRow, mOpacityPreference, mTextSizePreference, mTextColorPref,
            mBGColorPref, mCalendarColorPref, mAmPmMessanger, mShowCalendarIcon, mTommorowTextColor, mRoundedCorner;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        getCalendarEvents(context, intent);
    }

    public List<CalendarEvent> getEvents(Context context) {
        Cursor cursor = createLoadedCursor(context);
        if (cursor != null) {
            List<CalendarEvent> eventList = createEventList(cursor, context);
            cursor.close();
            return eventList;
        }
        return new ArrayList<CalendarEvent>();
    }

    private List<CalendarEvent> createEventList(Cursor calendarCursor, Context context) {
        List<CalendarEvent> eventList = new ArrayList<CalendarEvent>();
        for (int i = 0; i < calendarCursor.getCount(); i++) {
            calendarCursor.moveToPosition(i);
            CalendarEvent event = createCalendarEvent(calendarCursor);
            eventList.add(event);
        }
        return eventList;
    }

    private CalendarEvent createCalendarEvent(Cursor calendarCursor) {
        CalendarEvent event = new CalendarEvent();
        event.setId(calendarCursor.getInt(0));
        event.setTitle(calendarCursor.getString(1));
        event.setDateMillis(calendarCursor.getLong(2));
        event.setAllDay(calendarCursor.getLong(3) > 0);
        event.setDateEndMillis(calendarCursor.getLong(4));
        return event;
    }

    @SuppressLint("SimpleDateFormat")
    private void getCalendarEvents(final Context context, Intent intent) {
        List<CalendarEvent> CALENDAR_EVENTS = getEvents(context);

        try {
            if (CALENDAR_EVENTS.get(0).getFormatedTime() == null) {
                CALENDAR_EVENTS.remove(0);
            }
            start = CALENDAR_EVENTS.get(0).getDateMillis();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mCalendar = Calendar.getInstance();

        // Check the Shared Preference If User Changes the Color, value Or Not in preference
        mAmPmMessanger = context.getSharedPreferences(WIDGET_TIME_WITH_AM_PM, 0);

        mSharedPreferencesForEventRow = context.getSharedPreferences(NUMBER_OF_EVENTS, 0);
        row_counter = mSharedPreferencesForEventRow.getString(NUMBER_OF_EVENTS, NUMBER_OF_EVENTS_DEFAULT);

        mOpacityPreference = context.getSharedPreferences(WIDGET_BACKGROUND_OPACITY, 0);
        bagOpacity = mOpacityPreference.getString(WIDGET_BACKGROUND_OPACITY, WIDGET_BACKGROUND_OPACITY_DEFAULT);

        mTextSizePreference = context.getSharedPreferences(PREF_TEXT_SIZE, 0);
        textSize = mTextSizePreference.getString(PREF_TEXT_SIZE, PREF_TEXT_SIZE_DEFAULT);

        mTextColorPref = context.getSharedPreferences(PREF_TEXT_BACKGROUND_COLOR, 0);
        textColor = mTextColorPref.getString(PREF_TEXT_BACKGROUND_COLOR, PREF_TEXT_BACKGROUND_COLOR_DEFAULT);

        mBGColorPref = context.getSharedPreferences(PREF_WIDGET_BACKGROUND_COLOR, 0);
        BGColor = mBGColorPref.getString(PREF_WIDGET_BACKGROUND_COLOR, PREF_WIDGET_BACKGROUND_COLOR_DEFAULT);

        mCalendarColorPref = context.getSharedPreferences(PREF_CALENDAR_BACKGROUND_COLOR, 0);
        calendarColor = mCalendarColorPref.getString(PREF_CALENDAR_BACKGROUND_COLOR, PREF_CALENDAR_BACKGROUND_COLOR_DEFAULT);

        mShowCalendarIcon = context.getSharedPreferences(PREF_SHOW_CALENDAR, 0);
        showCalendar = mShowCalendarIcon.getString(PREF_SHOW_CALENDAR, PREF_SHOW_CALENDAR_DEFAULT);

        mTommorowTextColor = context.getSharedPreferences(PREF_TOMORROW_BACKGROUND_COLOR, 0);
        tomorowColor = mTommorowTextColor.getString(PREF_TOMORROW_BACKGROUND_COLOR, PREF_TOMORROW_BACKGROUND_COLOR_DEFAULT);

        mRoundedCorner = context.getSharedPreferences(PREF_WIDGET_ROUNDED_CORNER, 0);

        StringBuilder mColorBuilder = new StringBuilder();
        mColorBuilder.append("#").append(bagOpacity).append(BGColor.substring(1));
        SimpleDateFormat formater = new SimpleDateFormat("dd MMM hh:mm");
        Date mDate = new Date();
        String mdate = formater.format(mDate);
        Date dateCC;
        try {
            dateCC = formater.parse(mdate);
            mCalendar.setTime(dateCC);
        } catch (ParseException e1) {
            e1.printStackTrace();
        }


        Calendar cal = Calendar.getInstance();
        int dt = cal.get(Calendar.DATE);
        String day = cal.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault()).toUpperCase(Locale.ENGLISH);
        String month = cal.getDisplayName(Calendar.MONTH, Calendar.SHORT,
                Locale.getDefault());
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context
                .getApplicationContext());

        ComponentName thisWidget = new ComponentName(
                context.getApplicationContext(), CheckWidget.class);
        boolean isWidgetClicked = intent.getBooleanExtra("isWidgetClicked", false);
        int layoutId = R.layout.calendarlayout;
        if (isWidgetClicked) {
            layoutId = R.layout.widgetconfig;
            Date timerDate = new Date(System.currentTimeMillis() + 3000);
            new Timer().schedule(new TimerTask() {

                @Override
                public void run() {
                    updateAllWidgets(context);
                }
            }, timerDate);
        } else {
            layoutId = R.layout.calendarlayout;
        }

        RemoteViews remoteViews = new RemoteViews(context
                .getApplicationContext().getPackageName(), layoutId);


        if (mRoundedCorner.getBoolean(WIDGET_ROUNDED_CORNER, WIDGET_ROUNDED_CORNER_DEFAULT)) {

            remoteViews.setInt(R.id.layoutClickable, "setBackgroundColor", Color.TRANSPARENT);

            remoteViews.setInt(R.id.iv, "setColorFilter", Color.parseColor(mColorBuilder.toString()));


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                remoteViews.setInt(R.id.iv, "setImageAlpha", Color.alpha(Color.parseColor(mColorBuilder.toString())));
            } else {
                remoteViews.setInt(R.id.iv, "setAlpha", Color.alpha(Color.parseColor(mColorBuilder.toString())));
            }


        } else {
            remoteViews.setInt(R.id.iv, "setBackgroundColor", Color.TRANSPARENT);
            remoteViews.setInt(R.id.layoutClickable, "setBackgroundColor", Color.parseColor(mColorBuilder.toString()));

        }

        if (!row_counter.equals("")) {
            event_row_counter = Integer.parseInt(row_counter);
        } else {

        }
        switch (remoteViews.getLayoutId()) {


            case R.layout.calendarlayout:


                //if(CALENDAR_EVENTS.get(0).getFormatedTime() != null){
                RemoteViews childView;
                CalendarEvent mCalendarEvent;
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                Set<String> activeCalenders = prefs.getStringSet(PREF_ACTIVE_CALENDARS, new HashSet<String>(Arrays.asList("a", "b")));

                remoteViews.removeAllViews(R.id.tvEventHandler);
                for (int i = 0; i < event_row_counter; i++) {
                    childView = new RemoteViews(context
                            .getApplicationContext().getPackageName(), R.layout.eventhandler);
                    mCalendarEvent = new CalendarEvent();
                    try {
                        String amChecker = mAmPmMessanger.getString(WIDGET_TIME_WITH_AM_PM, WIDGET_TIME_WITH_AM_PM_DEFAULT);
                        mCalendarEvent = CALENDAR_EVENTS.get(i);

                        if (mCalendarEvent.getFormatedTime() != null) {
                            childView.setTextViewText(R.id.tvEventHandler, mCalendarEvent.getFormatedTime() + " " + mCalendarEvent.getTitle());
                            childView.setTextColor(R.id.tvEventHandler, Color.parseColor(textColor));
                            addtextSize(childView);
                        } else {

                        }


                        //For AM/PM
                        if (Boolean.parseBoolean(amChecker) == true) {
                            keyForAmPm = true;
                            childView.setTextViewText(R.id.tvEventHandler, mCalendarEvent.getTimeWithAM() + " " + mCalendarEvent.getTitle());
                            childView.setTextColor(R.id.tvEventHandler, Color.parseColor(textColor));
                            addtextSize(childView);
                        }

                        Date checkCurrentDate = new Date(mCalendarEvent.getDateMillis());

                        //Check For Today
                        if (checkCurrentDate != null) {


                            if (isToday(dayMonthYear(checkCurrentDate).get(2), dayMonthYear(checkCurrentDate).get(1),
                                    dayMonthYear(checkCurrentDate).get(0))) {


                                if (!mCalendarEvent.isAllDay()) {
                                    String timeWithoutAm = extractTime(mCalendarEvent.getTimeWithAM());
                                    String timeWithAm = extractTimeWithAM(mCalendarEvent.getTimeWithAM());

                                    if (keyForAmPm) {
                                        childView.setTextViewText(R.id.tvEventHandler, "Today" + " " + timeWithAm + " " + mCalendarEvent.getTitle());
                                        childView.setTextColor(R.id.tvEventHandler, Color.parseColor(calendarColor));
                                        addtextSize(childView);

                                    } else {
                                        childView.setTextViewText(R.id.tvEventHandler, "Today" + " " + timeWithoutAm + " " + mCalendarEvent.getTitle());
                                        childView.setTextColor(R.id.tvEventHandler, Color.parseColor(calendarColor));
                                        addtextSize(childView);
                                        //eventTime(timeWithoutAm, context);


                                    }
                                } else {
                                    childView.setTextViewText(R.id.tvEventHandler, "Today" + " " + mCalendarEvent.getTitle());
                                    childView.setTextColor(R.id.tvEventHandler, Color.parseColor(calendarColor));
                                }


                            }

                            //Check For Tomorrow
                            DateTime tomorrowDate = new DateTime(checkCurrentDate);
                            DateTime tomorrow = new DateTime().plusDays(1);
                            tomorrow.withTimeAtStartOfDay();
                            if (DateTimeComparator.getDateOnlyInstance().compare(tomorrowDate, tomorrow) == 0) {
                                if (!mCalendarEvent.isAllDay()) {
                                    String timeWithoutAm = extractTime(mCalendarEvent.getTimeWithAM());
                                    String timeWithAm = extractTimeWithAM(mCalendarEvent.getTimeWithAM());

                                    if (keyForAmPm) {
                                        System.out.println("tomorrow 1");
                                        childView.setTextViewText(R.id.tvEventHandler, "Tomorrow" + " " + timeWithAm + " " + mCalendarEvent.getTitle());
                                        childView.setTextColor(R.id.tvEventHandler, Color.parseColor(tomorowColor));
                                        addtextSize(childView);
                                    } else {
                                        System.out.println("tomorrow 2");
                                        childView.setTextViewText(R.id.tvEventHandler, "Tomorrow" + " " + timeWithoutAm + " " + mCalendarEvent.getTitle());
                                        childView.setTextColor(R.id.tvEventHandler, Color.parseColor(tomorowColor));
                                        addtextSize(childView);
                                    }
                                } else {
                                    System.out.println("tomorrow 3");
                                    childView.setTextViewText(R.id.tvEventHandler, "Tomorrow" + " " + mCalendarEvent.getTitle());
                                    childView.setTextColor(R.id.tvEventHandler, Color.parseColor(tomorowColor));
                                }

                            }

                            //The most hacking way
                            if (activeCalenders.isEmpty()) {
                                childView.setTextViewText(R.id.tvEventHandler, "");
                            }
                        }
                        remoteViews.addView(R.id.layoutParentView, childView);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                RemoteViews calendarIconView;
                remoteViews.removeAllViews(R.id.layoutCalendarImage);
                if (Boolean.parseBoolean(showCalendar) != false) {
                    calendarIconView = new RemoteViews(context
                            .getApplicationContext().getPackageName(), R.layout.calendarimageframeview);
                    calendarIconView.setTextViewText(R.id.textViewMonthInCalImage, month.toUpperCase(Locale.getDefault()));
                    calendarIconView.setTextViewText(R.id.textViewDateInCalImage, String.valueOf(dt));
                    calendarIconView.setTextViewText(R.id.textViewDayInCalImage, day);
                } else {
                    calendarIconView = new RemoteViews(context
                            .getApplicationContext().getPackageName(), R.layout.noimagecalendarframeview);
                    calendarIconView.setTextViewText(R.id.textViewDate, String.valueOf(dt));
                    calendarIconView.setTextViewText(R.id.textViewMonth, month.toUpperCase(Locale.getDefault()));
                    calendarIconView.setTextViewText(R.id.textViewDay, day);

                    calendarIconView.setTextColor(R.id.textViewDate, Color.parseColor(calendarColor));
                    calendarIconView.setTextColor(R.id.textViewMonth, Color.parseColor(calendarColor));
                    calendarIconView.setTextColor(R.id.textViewDay, Color.parseColor(calendarColor));
                }

                remoteViews.addView(R.id.layoutCalendarImage, calendarIconView);


                Intent clickIntent = new Intent(context.getApplicationContext(), CheckWidget.class);
                clickIntent.putExtra("isWidgetClicked", true);
                clickIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(context.getApplicationContext(), 0, clickIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                remoteViews.setOnClickPendingIntent(R.id.layoutClickable, pendingIntent);
                break;
            case R.layout.widgetconfig:
                remoteViews.setOnClickPendingIntent(R.id.buttonCalendarNew, openCalendar(context));

                //Config Button
                Intent i2 = new Intent(context, Calendar_Settings.class);
                i2.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                PendingIntent pi2 = PendingIntent.getActivity(context.getApplicationContext(),
                        0, i2, PendingIntent.FLAG_UPDATE_CURRENT);
                remoteViews.setOnClickPendingIntent(R.id.buttonSettingsNew, pi2);

                //Refresh Button
                Intent broadcastIntent = new Intent(context, CheckWidget.class);
                broadcastIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
                PendingIntent pi3 = PendingIntent.getBroadcast(context.getApplicationContext(),
                        0, broadcastIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                remoteViews.setOnClickPendingIntent(R.id.buttonRefreshNew, pi3);
                break;
        }


        final Intent i = new Intent(context, CheckWidget.class);
        final PendingIntent pending = PendingIntent.getBroadcast(context, 0, i, 0);
        final AlarmManager alarm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarm.cancel(pending);
        alarm.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
                SystemClock.elapsedRealtime() + 60000,
                AlarmManager.INTERVAL_FIFTEEN_MINUTES, pending);

        appWidgetManager.updateAppWidget(thisWidget, remoteViews);

    }

    private boolean isToday(int day, int mnth, int yr) {
        boolean isSameDay = false;
        Calendar c = Calendar.getInstance();

        // set the calendar to start of today
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);

        Date today = c.getTime();
        int year = yr;
        int month = mnth;
        int dayOfMonth = day;

        // reuse the calendar to set user specified date
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        // and get that as a Date
        Date dateSpecified = c.getTime();
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(today);
        cal2.setTime(dateSpecified);
        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
                && cal1.get(Calendar.DAY_OF_YEAR) == cal2
                .get(Calendar.DAY_OF_YEAR);
        if (sameDay) {
            isSameDay = true;
        }

        return isSameDay;
    }

    private ArrayList<Integer> dayMonthYear(Date date) {
        ArrayList<Integer> fetchedDayMonthYear = new ArrayList<Integer>();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int y = cal.get(Calendar.YEAR);
        fetchedDayMonthYear.add(y);
        int m = cal.get(Calendar.MONTH);
        fetchedDayMonthYear.add(m);
        int d = cal.get(Calendar.DAY_OF_MONTH);
        fetchedDayMonthYear.add(d);
        return fetchedDayMonthYear;
    }


    private String createSelectionClause(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> activeCalenders = prefs.getStringSet(PREF_ACTIVE_CALENDARS,
                new HashSet<String>());
        if (activeCalenders.isEmpty()) {
            return EVENT_SELECTION;
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(AND_BRACKET);
        Iterator<String> iterator = activeCalenders.iterator();
        while (iterator.hasNext()) {
            String calendarId = iterator.next();
            stringBuilder.append(Instances.CALENDAR_ID);
            stringBuilder.append(EQUALS);
            stringBuilder.append(calendarId);
            if (iterator.hasNext()) {
                stringBuilder.append(OR);
            }
        }
        stringBuilder.append(CLOSING_BRACKET);

        return EVENT_SELECTION + stringBuilder.toString();
    }

    private Cursor createLoadedCursor(Context context) {
        SharedPreferences listPrefs = context.getSharedPreferences(PREF_EVENT_RANGE, 0);
        int dateRange = Integer
                .valueOf(listPrefs.getString(PREF_EVENT_RANGE, PREF_EVENT_RANGE_DEFAULT));
		/*SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		int dateRange = Integer
				.valueOf(prefs.getString(PREF_EVENT_RANGE, PREF_EVENT_RANGE_DEFAULT));*/
        long start = System.currentTimeMillis();
        long end = start + DateUtils.DAY_IN_MILLIS * dateRange;
        Uri.Builder builder = Instances.CONTENT_URI.buildUpon();
        ContentUris.appendId(builder, start);
        ContentUris.appendId(builder, end);
        String selection = createSelectionClause(context);
        ContentResolver contentResolver = context.getContentResolver();
        return contentResolver.query(builder.build(), PROJECTION, selection, null, EVENT_SORT_ORDER);
    }

    public static void updateAllWidgets(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        ComponentName compName = new ComponentName(context, CheckWidget.class);
        Intent intent = new Intent(context, CheckWidget.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, appWidgetManager.getAppWidgetIds(compName));
        context.sendBroadcast(intent);
    }

    private PendingIntent openCalendar(final Context context) {
        long startMillis = start;
        Intent i = new Intent(context, ShowCalendar.class);
        i.putExtra("CURRENT_EVENT_TIME", startMillis);
        PendingIntent pi = PendingIntent.getActivity(context.getApplicationContext(),
                0, i, PendingIntent.FLAG_UPDATE_CURRENT);
        return pi;
    }

    private String extractTime(String date) {
        System.out.println("date without am" + date);
        SimpleDateFormat parseFormat = new SimpleDateFormat("dd MMM hh:mm aa");
        SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm");
        Date dt = null;
        try {
            dt = parseFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return printFormat.format(dt);
    }

    private String extractTimeWithAM(String date) {
        System.out.println("date with am" + date);
        SimpleDateFormat parseFormat = new SimpleDateFormat("dd MMM hh:mm a");
        SimpleDateFormat printFormat = new SimpleDateFormat("hh:mm a");
        Date dt = null;
        try {
            dt = parseFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return printFormat.format(dt);
    }

    @SuppressLint("NewApi")
    private void addtextSize(RemoteViews rv) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            rv.setTextViewTextSize(R.id.tvEventHandler, TypedValue.COMPLEX_UNIT_SP, Float.parseFloat(textSize));
        }
    }


    private void eventTime(String event_time, Context context) {
        Calendar cal = Calendar.getInstance();
        Date currentLocalTime = cal.getTime();
        Format date = new SimpleDateFormat("HH:mm");
        System.out.println("date is " + date.format(currentLocalTime));
        if (event_time.equalsIgnoreCase(date.format(currentLocalTime))) {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            MediaPlayer mp = MediaPlayer.create(context.getApplicationContext(), notification);
            mp.start();
        }

    }


}
