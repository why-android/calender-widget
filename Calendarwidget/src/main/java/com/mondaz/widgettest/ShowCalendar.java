package com.mondaz.widgettest;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
public class ShowCalendar extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		long startMillis = getIntent().getLongExtra("CURRENT_EVENT_TIME", 0);
		Uri.Builder builder = CalendarContract.CONTENT_URI.buildUpon();
		builder.appendPath("time");
		ContentUris.appendId(builder, startMillis);						
		Intent i = new Intent(Intent.ACTION_VIEW).setData(builder.build());
		startActivity(i);
		CheckWidget.updateAllWidgets(this);
		finish();
	}

}
